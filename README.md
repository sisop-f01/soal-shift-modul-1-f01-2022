# Soal Shift Modul 1
Anggota Kelompok :
1. Angela Oryza Prabowo (5025201022)
2. Azzura Ferliani Ramadhani (5025201190)
3. Naufal Adli Purnama (5025201195)

## Soal-1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

- `a.`Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file `./users/user.txt`. Han juga membuat sistem login yang dibuat di script `main.sh`
- `b.`Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut:  
  - Minimal 8 karakter </br>
  - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
  - Alphanumeric
  - Tidak boleh sama dengan username
- `c.`Setiap percobaan login dan register akan tercatat pada `log.txt` dengan format :
MM/DD/YY hh:mm:ss **MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.
  - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah **REGISTER: ERROR User already exists**
  - Ketika percobaan register berhasil, maka message pada log adalah **REGISTER: INFO User USERNAME registered successfully**
  - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah **LOGIN: ERROR Failed login attempt on user USERNAME**
  - Ketika user berhasil login, maka message pada log adalah **LOGIN: INFO User USERNAME logged in**
- `d.`Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
  - dl N ( N = Jumlah gambar yang akan didownload)\
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama `YYYY-MM-DD_USERNAME`. Gambar-gambar yang didownload juga memiliki format nama `PIC_XX`, dengan nomor yang berurutan `(contoh : PIC_01, PIC_02, dst.)`. Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
  - att\
  Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

## Penyelesaian Soal-1
Untuk menyelesaikan soal ini, kita perlu membuat dua file yaitu `register.sh` dan `main.sh`. Dimana file `register.sh` berisi sistem *register* dengan berbagai syarat *password* yang tertera pada poin `b` dan setelahnya jika *user* berhasil didaftarkan, akan dimasukkan ke dalam file `./users/user.txt`. Selain itu juga perlu dibuat file `main.sh` yang berisi sistem *login* dan juga dua *command* yaitu **dl N** dan **att**.
  
1. `a`
> Pertama-tama dibuat kedua file yakni `register.sh` dan `main.sh`.
```bash
touch register.sh
touch main.sh
```
> Khusus untuk file `user.txt` karena berada di folder yang berbeda, maka perlu dibuat foldernya terlebih dulu baru setelahnya membuat file pada folder tersebut.
```bash
filename="./users/user.txt"

if [ ! -d "./users" ]
then 
	mkdir -p ./users
fi

if [ ! -f "$filename" ]
then 
	touch "$filename"
fi

```
2. `b`
> Masing-masing syarat dibuat if satu-persatu sehingga *user* dapat mengetahui letak dari kesalahan *password*.
```bash
if [[ "$passwd" == "$uname" ]]
then 
	flag=1
	echo "$dt REGISTER:ERROR Username and password cannot be the same." >> log.txt
	echo "$dt REGISTER:ERROR Username and password cannot be the same."
fi
if [[ $len -lt $minlen ]]
then
	flag=1
	echo "$dt REGISTER:ERROR Password must be a minimum of 8 characters." >> log.txt
	echo "$dt REGISTER:ERROR Password must be a minimum of 8 characters."
fi
if [[ "$passwd" =~ [^a-zA-Z0-9] ]]
then 
	flag=1
	echo "$dt REGISTER:ERROR Password must only consist of alphanumeric characters." >> log.txt
	echo "$dt REGISTER:ERROR Password must only consist of alphanumeric characters."
fi
if [[ ! "$passwd" =~ [a-z] ]] || [[ ! "$passwd" =~ [A-Z] ]] 
then
	flag=1
	echo "$dt REGISTER:ERROR Password must contain at least 1 uppercase and 1 lowercase character." >> log.txt
	echo "$dt REGISTER:ERROR Password must contain at least 1 uppercase and 1 lowercase character."
fi
```
3. `c.`
> Untuk mengetahui apakah sudah ada *username* yang sama, perlu dilakukan pengecekan pada file `user.txt` dengan menggunakan script grep setelahnya apabila ada *username* yang sama maka akan dikeluarkan error.
```bash
if ! [[ -z $(cat users/user.txt | grep -w $tempusr) ]]  
then
	echo "$dt REGISTER:ERROR User already exists" >> log.txt
	echo "$dt REGISTER:ERROR User already exists" 
	exit 1
fi
```
> Pada file `register.sh` terdapat flag untuk mengecek apakah seluruh syarat untuk membuat *user* baru terpenuhi dan ketika flag 0 *user* baru akan terdaftar.
```bash
if [[ $flag -eq 0 ]]
then
	echo "$dt REGISTER:INFO User $usrname registered successfully." >> log.txt
	echo "$dt REGISTER:INFO User $usrname registered successfully."
	echo "$uname $passwd" >> $filename 
fi
```
> Untuk sistem login, pertama perlu dicek terlebih dahulu apakah *username* tercantum atau tidak pada file `user.txt` menggunakan grep. Apabila *username* belum tersedia, akan ada error yang dikeluarkan.
```bash
if [[ -z $(cat users/user.txt | grep "$usrnm ")  ]] 
then 
	echo "$dt LOGIN:ERROR Username $usrnm does not exist."
	exit 1
else
	read -s -p "Password: " psswd
	echo
fi
```
> Setelahnya, baru dicek apakah *username* dengan *password*nya sama atau tidak, juga menggunakan grep. Apabila *username* dan *password* berbeda, akan keluar error. Sebaliknya, jika *username* dan *password* sudah sinkron maka akan dikeluarkan info login
```bash
if [[ -z $(cat users/user.txt | grep -w "$usrnm $psswd" ) ]] 
then
	echo "$dt LOGIN:ERROR Failed login attempt on user $usrnm." >> log.txt
	echo "$dt LOGIN:ERROR Failed login attempt on user $usrnm." 
	flag=1
else
	echo "$dt LOGIN:INFO User $usrnm logged in." >> log.txt
	echo "$dt LOGIN:INFO User $usrnm logged in."
	flag=0
fi

```
4. `d.`
> Untuk *command* **dl N**
>> Perlu diambil terlebih dahulu berapa jumlah N.
```bash 
dwn_amt=$(echo "$command" | awk '{printf "%d",$2}')
```
>> setelahnya perlu dibuat folder dengan ketentuan `YYYY-MM-DD_USERNAME` juga dibuat dengan chmod 777 agar kita dapat melakukan apapun terhadap folder.
```bash
filename=$(echo "`date +%F`_$usrnm")
mkdir "$filename"
chmod 777 "$filename"
```
>> Lalu melakukan perulangan untuk men*download* gambar. Untuk men*download* gambar, digunakan curl. Dengan menggunakan format penamaan `PIC_XX` file yang telah di*download* disimpan pada folder yang tadi sudah dibuat.
```bash
n=0
while [[ $n -lt $dwn_amt ]]
do
	let n=$n+1
	let a=($n+$amt)/10
	let b=($n+$amt)%10
	curl 'https://loremflickr.com/320/240' -o "$filename/PIC_$a$b.jpg"
	echo $n
done
```
>> Untuk kondisi zip telah tersedia, perlu dilakukan unzip terlebih dahulu.
```bash
unzip "$filename.zip"
```
>> Agar tidak terjadi *rewrite* gambar, maka perlu diambil terlebih dahulu berapa jumlah yang ada di zip yang telah ada sebelumnya yakni dengan menggunakan `awk`.
```bash
amt=$(ls "$filename" | awk 'END{printf "%d",NR}')
```
>> Setelah seluruh proses selesai, folder akan kembali di zip dan juga menggunakan *password* untuk *protection*.
```bash
zip -P $psswd -r "$filename.zip" "$filename"
```
> Untuk *command* **att**
>> Dengan menggunakan fungsi `grep` dan menghitung jumlah info LOGIN dari *user* menggunakan `awk`.
```bash
cat log.txt | grep "LOGIN" | grep "$usrnm" | awk 'END {print NR}'
```
## Dokumentasi Soal-1
![Hasil menjalankan `register.sh`](/Dokumentasi/Soal1/register_success.png) <br>
*Hasil menjalankan `register.sh`* <br>
![Error salah password pada `register.sh`](/Dokumentasi/Soal1/register_error_example.png) <br>
*Error salah password pada `register.sh`* <br>
![Error salah username pada `main.sh`](/Dokumentasi/Soal1/main_wrong_uname.png) <br>
*Error salah username pada `main.sh`* <br>
![Error salah password pada `main.sh`](/Dokumentasi/Soal1/main_wrong_pass.png) <br>
*Error salah password pada `main.sh`* <br>
![Hasil menjalankan `main.sh`](/Dokumentasi/Soal1/main_full.png) <br>
*Hasil menjalankan `main.sh* <br>
![Command dl ketika perlu unzip](/Dokumentasi/Soal1/dl_unzip.png) <br>
*Command dl ketika perlu unzip* <br>

## Kendala Soal-1
Awalnya, pada saat register, masih ada sedikit kekurangan. Yakni *username* tidak bisa sama dengan *password* yang telah terdaftar. Saat ini, kesalahan tersebut telah diperbaiki.

## Soal-2

Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

- `a.` Buat folder terlebih dahulu bernama `forensic_log_website_daffainfo_log.` <br>
- `b.` Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama `ratarata.txt` ke dalam folder yang sudah dibuat sebelumnya. <br>
- `c.` Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama `result.txt` kedalam folder yang sudah dibuat sebelumnya.<br>
- `d.`Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? <br>
- `e.` Kemudian masukkan berapa banyak requestnya kedalam file bernama `result.txt` yang telah dibuat sebelumnya.
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama `result.txt` yang telah dibuat sebelumnya. <br>

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:

- `File ratarata.txt` <br>
    Rata-rata serangan adalah sebanyak **<ins>rata_rata</ins>** requests per jam <br>
- `File result.txt` <br>
    IP yang paling banyak mengakses server adalah: **<ins>ip_address</ins>** sebanyak **<ins>jumlah_request</ins>** requests <br>

    Ada **<ins>jumlah_req_curl</ins>** requests yang menggunakan curl sebagai user-agent

    **<ins>IP Address Jam 2 pagi</ins><br>**
    **<ins>IP Address Jam 2 pagi</ins><br>**
    **<ins>dst</ins>**

## Penyelesaian Soal-2
Untuk menyelesaikan soal nomor 2, logika yang digunakan adalah mencari informasi yang diminta soal pada file `forensic_log_website_daffainfo_log` dan melakukan operasi pada informasi tersebut. Command yang digunakan dalam pencarian informasi adalah **`awk`**

1. `a` <br>
> Untuk membuat folder pada shell-scripting dapat memanfaatkan bantuan command `mkdir` kemudian diikuti dengan nama file yang ingin dibuat. `mkdir` sendiri merupakan singkatan dari *make directory*
```bash
mkdir forensic_log_website_daffainfo_log
```
2. `b` <br>
> - Untuk mendapatkan informasi rata-rata banyaknya serangan setiap jam, kita perlu mencari informasi terkait waktu serangan pada file script yang sudah tersedia. Dalam hal ini, command `awk` akan dijalankan secara bertahap untuk memisahkan mana informasi yang dibutuhkan dan mana yang tidak. 
> - Langkah pertama, `awk` akan memisahkan tanggal beserta waktu serangan dari informasi lainnya, seperti Alamat IP, dengan indikator pemisah `:"`. 
> - Setelah mendaptkan informasi tanggal dan waktu, karena soal hanya meminta rata-rata serangan per jam, maka informasi berikutnya yang perlu dihilangkan adalah bulan dan tahun. Untuk menghilangkan bulan dan tahun, kemudian digunakan indikator pemisah `/`. 
> - Langkah terakhir adalah menghilangkan informasi menit dan detik, sehingga yang tersisa hanya tanggal dan jam. Dalam kasus ini, maka digunakan indikator pemisah `:` 
> - Setelah semua informasi yang dibutuhkan sudah terkumpul, maka informasi tersebuut di simpan ke sebuah *file temporary* yang diberi nama `temp_date_time`
```bash
cat log_website_daffainfo.log | awk -F ":\"" '{print $2}' |  awk -F "/" '{print $1,$3}'
| awk -F ":" '{print $1, $2}' | awk -F" " '{print $1,$3}' > temp_date_time
```
> - Kemudian, untuk mencari rata-rata dari banyaknya serangan tiap jam, maka kita perlu mengetahui terlebih dahulu rentang waktu dari serangan pertama hingga serangan terakhir
> - Setelah rentang waktu serangan berhasil disimpan, langkah berikutnya adalah membagi banyaknya serangan dengan rentang watu tersebut. Banyaknya serangan dapat diketahui dari jumlah baris dikurangi 1 karena baris pertama merupakan judul kolom
> - Hasil perhitungan rata-rata kemudian akan disimpan ke dalam sebuah file bernama `ratarata.txt`
> - Karena *file temporary* `temp_date_time` sudah tidak digunakan lagi, maka fie tersebut dapat dihapus dengan menggunakan command `rm` atau *remove*
```bash
cat temp_date_time | awk 'NR==2{d1=$1; t1=$2} END{d2=$1; t2=$2; 
print "Rata-rata serangan adalah sebanyak "(NR-1)/(t2+24*(d2-d1)-t1+1)" requests per jam"}' > $fileavg

rm temp_date_time
```
3. `c`
> - Untuk mencari IP Adress yang paling banyak mengakses website, maka kita perlu memilah dan memisahkan informasi terkait IP Adress dari informasi lainnya. Hal tersebut dapat dilakukan dengan menggunakan command `awk` dan indikator pemisah `"`
> - Setelah semua IP Adress tersimpan, banyaknya kemunculan IP Adress tersebut dalam file terkait kemudian dihitung dan disimpan dalam sebuah `array`
> - IP Adress yang paling banyak mengakses website dapat diketahui dengan cara membandingkan jumlah kemunculan  setiap IP Adress hingga ditemukan IP Adress dengan jumlah kemunculan terbanyak
> - Informasi tersebut disimpan ke dalam file bernma `result.txt`
```bash
cat log_website_daffainfo.log | awk -F '"' '{print $2}' | awk '{a[$1]++} END{for (n in a) print a[n], n}' 
| awk 'BEGIN{max=0} $1 > max{max=$1} END{print "IP yang paling banyak mengakses server adalah: "$2", sebanyak "max" requests"}'
> $fileres
```
4. `d`
> - Untuk mencari jumlah *request* yang menggunakan `curl` sebagai *user-agent*, dapat menggunakan command `awk` dengan bantuan regular expression `/curl/`
> - Setip ditemukan baris yang mengandung kata **curl**, sistem akan melakukan inkrementasi pada variabel **n**. Untuk menampilkan jumlah **curl** yang muncul, maka sistem akan menampilkan jumlah **n**
> - Informasi tersebut akan disimpan ke dalam file bernama `result.txt`
```bash
cat log_website_daffainfo.log | awk '/curl/ {n++} END{print "Ada "n" requests yang menggunakan curl sebagai user-agent"}'
>>$fileres
```
5. `e`
> - Untuk menampilkan informasi mengenai IP Adress yang mengakses website pada pukul 02.00 pagi, maka kita perlu memisahkan informasi mengenai IP Adress dan tanggal akses website dari informasi lainnya. Agar hal ini dapat terealisasi, maka digunakan bantuan command `awk` dengan indikator pemisah `"`
> - Setelah itu, karena tanda `/` dapat mengganggu pencarian `awk` dengan menggunakan regular expression, maka langkah selanjutnya adalah memilah informasi dengan indikator pemisah `/`
> - Karena kita hanya membutuhkan informasi jam pada waktu, maka informasi mengenai menit dan detik waktu akses dapat dihilangkan dengan menggunakan indikator pemisah `:`
> - Untuk menampilkan IP Adress yang mengakses website pada pukul 02.00 pagi, digunakan bantuan regular expression pada command `awk` berupa `/22 Jan 2022 02/`
> - Sistem akan menampilkan semua IP Adress yang mengakses website pada jam 02.00 pagi, sehingga untuk menghindari duplikasi, maka ditambahkan sebuah perintah. Perintah tambahan tersebut, yaitu sistem hanya perlu menampilkan IP Adress jika IP Adress tersebut belum pernah ditampilkan sebelumnya.
> - Informasi tersebut akan disimpan ke dalam file bernama `result.txt`
```bash
cat log_website_daffainfo.log | awk -F '"' '{print $2,$4}' | awk -F '/' '{print $1,$2,$3}'
| awk -F ':' '{print $1,$2}'| awk '/22 Jan 2022 02/ {print""$1" Jam 2 Pagi"}' | awk '!a[$0]++'>> $fileres
```
## Dokumentasi Soal-2

![Shell script untuk memanggil `soal2_forensic_dapos.sh` ](/Dokumentasi/Soal2/sebelum_script.png) <br>
*Shell script untuk memanggil `soal2_forensic_dapos.sh`* <br>

![Isi dari file `ratarata.txt`](/Dokumentasi/Soal2/isi_ratarata.png) <br>
*Isi dari file `ratarata.txt`* <br>

![Isi dari file `result.txt`](/Dokumentasi/Soal2/isi_result.png) <br>
*Isi dari file `result.txt`* <br>

## Kendala Soal-2
Kendala terkait file `bash` ini adalah file ini masih belum mampu menyelesaikan permasalahan bila file script informasi website memuat data dari berbagai bulan dan tahun. File atau sistem ini hanya bersifat valid jika informasi yang dimuat masih berada pada jangka satu bulan

## Soal-3

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

- `a.` Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.<br>
- `b.` Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.<br>
- `c.` Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log<br>
- `d.` Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.<br>

Note:
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
```
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```
Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
```
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62
```

## Penyelesaian Soal-3
Logika yang digunakan adalah untuk membuat file log menitan sesuai dengan soal tetapi juga untuk meng-append isi dari file tersebut ke file yang akan menjadi log per jamnya. Hal ini dilakukan dengan harapan dapat memudahkan operasi agregasi pada data menitan karena data tersebut sudah berada dalam satu file yang sama. Dalam satu jam, akan ada 61 file baru, yaitu 60 file log menitan serta satu file agregasi jam yang berisi 60 baris data log menitan. Data dalam file agregasi jam selanjutnya akan dicari nilai maximum, minimum, dan average dengan menggunakan AWK untuk mengiterasi setiap baris sehingga dari 61 file baru, 60 file log menitan tetap sama dan file agregasi jam sekarang berisis data maximum, minimum, dan average dari setiap kolom data.

1. `a` 
    ```bash
    mkdir -p ~/log

    str1=$(free -m | awk 'BEGIN{OFS=","} NR!=1 {for(i=2;i<=NF;i++) printf $i",";}')
    str2=$(du -sh ~ | awk 'BEGIN{OFS=","} {print $2,$1}')
    dtFull=$(date +"%Y%m%d")
    dtTime=$(date +"%H%M%S")
    dtHour=$(date +"%H")

    if [[ -f ~/log/metrics_agg_$dtFull$dtHour.log ]]
    then
    chmod 600 ~/log/metrics_agg_$dtFull$dtHour.log
    fi
    echo "$str1$str2" >> ~/log/metrics_$dtFull$dtTime.log
    cat ~/log/metrics_$dtFull$dtTime.log >> ~/log/metrics_agg_$dtFull$dtHour.log
    ```
>Soal ini diselesaikan dengan memanfaatkan AWK untuk mengekstrak output dari command `free -m` dan `du -sh ~` agar   mendapatkan datanya saja. Setelah itu, `$(date +"%Y%m%d")` dan `$(date +"%H%M%S")` digunakan untuk menyimpan tanggal dan waktu ke dalam variabel masing-masing agar dapat digunakan untuk penamaan file. Pernyataan kondisional digunakan untuk memastikan hak akses yang memungkinan penulisan pada file agregasi. Setelah itu, output dari command `free -m` dan `du -sh ~` dimasukkan ke dalam file log menitan dan di-append ke dalam file log per jam.

2. `b` 
>Digunakan crontab berisi cronjob seperti berikut untuk menjalankan script minute_log.sh setiap menit

    ```
    * * * * * /{insert directory here}/minute_log.sh
    ```

>Sebelum itu dipastikan akses eksekusi file minute_log.sh diberikan dengan:

    ```
    chmod +x /{insert directory here}/minute_log.sh
    ```

3. `c`
    ```bash
    mkdir -p ~/log

    dtDate=$(date --date="1 hour ago" +"%Y%m%d");
    dtHour=$(date --date="1 hour ago" +"%H");
    awk -F"," '
    BEGIN{OFS=",";min1=99999;min2=99999;min3=99999;min4=99999;min5=99999;min6=99999;min7=99999;min8=99999;min9=99999;min11=99999} 
    $1 < min1 {min1=$1;}
    $2 < min2 {min2=$2;}
    $3 < min3 {min3=$3;}
    $4 < min4 {min4=$4;}
    $5 < min5 {min5=$5;}
    $6 < min6 {min6=$6;}
    $7 < min7 {min7=$7;}
    $8 < min8 {min8=$8;}
    $9 < min9 {min9=$9;}
    $11 < min11 {min11=$11;}
    END{print "minimum",min1,min2,min3,min4,min5,min6,min7,min8,min9,$10,min11}' ~/log/metrics_agg_$dtDate$dtHour.log > temp$dtDate$dtHour.log


    awk -F"," '
    BEGIN{OFS=",";max1=0;max2=0;max3=0;max4=0;max5=0;max6=0;max7=0;max8=0;max9=0;max11=0}
    $1 > max1 {max1=$1}
    $2 > max2 {max2=$2}
    $3 > max3 {max3=$3}
    $4 > max4 {max4=$4}
    $5 > max5 {max5=$5}
    $6 > max6 {max6=$6}
    $7 > max7 {max7=$7}
    $8 > max8 {max8=$8}
    $9 > max9 {max9=$9}
    $11 > max11 {max11=$11}
    END{print "maximum",max1,max2,max3,max4,max5,max6,max7,max8,max9,$10,max11}' ~/log/metrics_agg_$dtDate$dtHour.log >> temp$dtDate$dtHour.log

    awk -F"," 'BEGIN{OFS=",";sum1=0;sum2=0;sum3=0;sum4=0;sum5=0;sum6=0;sum7=0;sum8=0;sum9=0;sum11=0;}
    {sum1+=$1;sum2+=$2;sum3+=$3;sum4+=$4;sum5+=$5;sum6+=$6;sum7+=$7;sum8+=$8;sum9+=$9;sum11+=$11}
    END{sum1/=NR;sum2/=NR;sum3/=NR;sum4/=NR;sum5/=NR;sum6/=NR;sum7/=NR;sum8/=NR;sum9/=NR;sum11/=NR;print "average",sum1,sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,$10,sum11}
    ' ~/log/metrics_agg_$dtDate$dtHour.log >> temp$dtDate$dtHour.log

    chmod 700 ~/log/metrics_agg_$dtDate$dtHour.log
    cat temp$dtDate$dtHour.log > ~/log/metrics_agg_$dtDate$dtHour.log
    chmod 400 ~/log/metrics_agg_$dtDate$dtHour.log

    rm temp$dtDate$dtHour.log
    ```

>Soal ini diselesaikan dengan menggunakan AWK untuk mengiterasi setiap baris yang ada pada file agregasi untuk mencari nilai maximum, minimum, dan average dari semua metric selama satu jam terakhir. Terdapat tiga perintah AWK, masing-masing untuk mencari nilai maximum, minimum, dan average. Namun, ketiganya memiliki struktur yang sama, yaitu:

    ```
    awk -F"," '
    BEGIN{OFS=","; inisialisasi variabel setiap kolom}'
    kondisi {operasi}
    END{print hasil}
    '
    ```
>Hal yang beda adalah kondisi dan operasi, dimana maximum perlu diganti jika field pada baris saat ini lebih besar, minimum perlu diganti jika field pada baris saat ini lebih kecil, dan average hanya menambahkan pada sum. Pada operasi `END` sum perlu dibagi dengan jumlah baris yang ada di file agar mendapatkan nilai rata-rata.

4. `d` 
>Diselesaikan dengan menggunakan command seperti berikut untuk mengubah hak akses menjadi read-only:
    
    ```
    chmod 400 /direktori/nama_file.log
    ```

## Dokumentasi Soal-3

![Hasil `ls ~/log` sebelum dijalankan script](/Dokumentasi/Soal3/sebelum_script.png)*Hasil `ls ~/log` sebelum Dijalankan Script*

![Hasil `ls ~/log` setelah dijalankan script](/Dokumentasi/Soal3/setelah_script.png)*Hasil `ls ~/log` setelah Dijalankan Script*

![Contoh Isi Log Menitan](/Dokumentasi/Soal3/contoh_isi_minute_log.png)*Contoh Isi Log Menitan*

![Contoh Isi Log per Jam](/Dokumentasi/Soal3/contoh_isi_file_agregasi.png)*Contoh Isi Log per Jam*

## Kendala Soal-3
Kendala utama yang dihadapi praktikan adalah apabila terjadi suatu error, error tersebut susah di-debug karena ketidakpengalaman dalam pemrograman shell script dan AWK serta tidak adanya linter di dalam terminal sehingga perlu banyak waktu dan ketelitian untuk mencari kesalahan syntax. Seringkali kesalahan yang terjadi disebabkan ketidaktelitian seperti kekurangan karakter "'" dalam AWK dan "[" dalam pernyataan kondisional. Kendala kedua yang dihadapi praktikan merupakan format fungsi `date` untuk menyesuaikan dengan ketentuan soal. Namun, ada referensi di Google yang membantu menyelesaikan masalah tersebut. 
